﻿namespace Calicosol
{
   
    public class Contact
    {
        public int PkContactId { get; set; }

        public int FkCountyId { get; set; }

        public string FkContactTypeId { get; set; }

        public int Id { get; set; }

        public string IndividualBusiness { get; set; }

        public string Name { get; set; }

        public string Attention { get; set; }

        public string MailingAddress1 { get; set; }

        public string MailingAddress2 { get; set; }

        public string MailingCity { get; set; }

        public string MailingState { get; set; }

        public string MailingZip { get; set; }

        public string PhysicalAddress1 { get; set; }

        public string PhysicalAddress2 { get; set; }

        public string PhysicalState { get; set; }

        public string PhysicalCity { get; set; }

        public string PhysicalZip { get; set; }

        public string PhoneMain { get; set; }

        public string PhoneOther { get; set; }

        public string Cell { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }

        public int Active { get; set; }

        public string Comments { get; set; }

        public string FkUserId { get; set; }

        public string OtherInfo { get; set; }

        public int FkDistrictId { get; set; }
    }
}
