﻿namespace Calicosol
{
    public class ContactWithPermit : Contact
    {
        public string PermitNumber { get; set; }
    }
}
