﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Calicosol
{
    public partial class DemoApp : Application
    {
        public DemoApp()
        {
            InitializeComponent();
            var page = new ButtonDemoPage();
            page.BindingContext= new ButtonDemoViewModel();
            MainPage = page;
        }
    }
}
